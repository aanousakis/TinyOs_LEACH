#ifndef SIMPLEROUTINGTREE_H
#define SIMPLEROUTINGTREE_H


enum{
	SENDER_QUEUE_SIZE=5,
	RECEIVER_QUEUE_SIZE=3,
	AM_SIMPLEROUTINGTREEMSG=22,
	AM_ROUTINGMSG=22,
	AM_NOTIFYPARENTMSG=12,
	AM_IAMLEADERMSG=33,
	AM_DATAMSG=66,


	MAX_DEPTH=20,
	INIT_TIME_PER_DEPTH=100,  //poso xrono thelei kathe epipedo gia na labei routing msg kai na arxikopoiithei

	TREE_INIT=MAX_DEPTH*INIT_TIME_PER_DEPTH,//poso xrono thelei olo to dentro gia na arxikopoiithei

	EPOXI=60000,
	GUROS=300000,
	GUROI_EPAN=20,   //posous gurous tha exoume

	SelectLeader=EPOXI/5,    //o xronos gia tin epilogi tou cluster leader
	SendData=EPOXI/5,    //o xronos gia tin apostolh ton metriseon ston cluster leader
	ForwardData=EPOXI-SelectLeader-SendData,

	transmissionWindow=ForwardData/MAX_DEPTH,    //to xroniko parathuro gia tin apostoli apo tous cluster leaders sti riza 

	MAXLEADER=10,  //megistos arithmos upopsifion leader 


	BOOT_DELAY=200  
};

typedef nx_struct RoutingMsg
{
	nx_uint16_t senderID;
	nx_uint8_t depth;
} RoutingMsg;

typedef nx_struct IAMLEADERMsg
{
	//nx_uint16_t senderID;
} IAMLEADERMsg;

typedef nx_struct DATAMsg
{
	nx_uint16_t data;
} DATAMsg;

typedef nx_struct NotifyParentMsg
{
	nx_uint16_t senderID;
	nx_uint16_t parentID;
	nx_uint8_t  depth;
	nx_uint16_t sumx;
	nx_uint16_t sum2x;
	nx_uint16_t cnt;
} NotifyParentMsg;

#endif
