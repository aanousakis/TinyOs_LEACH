#include "SimpleRoutingTree.h"

configuration SRTreeAppC @safe() { }
implementation{
	components SRTreeC;


	components MainC, ActiveMessageC;
	components new TimerMilliC() as RoutingMsgTimerC;
	components new TimerMilliC() as RoundTimerC;            //<-----------------------
	components new TimerMilliC() as SelectLeaderTimerC;     //<-----------------------
	components new TimerMilliC() as SendDataTimerC;         //<-----------------------
	components new TimerMilliC() as ForwardDataTimerC;      //<-----------------------
	components new TimerMilliC() as SendDataRandomTimerC;   //<-----------------------
	components new TimerMilliC() as LeaderResponceTimerC;   //<-----------------------

	
	components new AMSenderC(AM_ROUTINGMSG) as RoutingSenderC;
	components new AMReceiverC(AM_ROUTINGMSG) as RoutingReceiverC;
	components new AMSenderC(AM_NOTIFYPARENTMSG) as NotifySenderC;
	components new AMReceiverC(AM_NOTIFYPARENTMSG) as NotifyReceiverC;
	components new AMSenderC(AM_IAMLEADERMSG)   as IAMLEADERSenderC;      //<-----------------------
	components new AMReceiverC(AM_IAMLEADERMSG) as IAMLEADERReceiverC;    //<-----------------------
	components new AMSenderC(AM_DATAMSG)        as DATASenderC;           //<-----------------------
	components new AMReceiverC(AM_DATAMSG)      as DATAReceiverC;          //<-----------------------


	components new PacketQueueC(SENDER_QUEUE_SIZE) as RoutingSendQueueC;
	components new PacketQueueC(RECEIVER_QUEUE_SIZE) as RoutingReceiveQueueC;
	components new PacketQueueC(SENDER_QUEUE_SIZE) as NotifySendQueueC;
	components new PacketQueueC(RECEIVER_QUEUE_SIZE) as NotifyReceiveQueueC;
	components new PacketQueueC(SENDER_QUEUE_SIZE)   as IAMLEADERSendQueueC;     //<-----------------------
	components new PacketQueueC(RECEIVER_QUEUE_SIZE) as IAMLEADERReceiveQueueC;  //<-----------------------
	components new PacketQueueC(SENDER_QUEUE_SIZE)   as DATASendQueueC;          //<-----------------------
	components new PacketQueueC(RECEIVER_QUEUE_SIZE) as DATAReceiveQueueC;       //<-----------------------
	
	SRTreeC.Boot->MainC.Boot;
	
	SRTreeC.RadioControl -> ActiveMessageC;

	SRTreeC.RoutingMsgTimer->RoutingMsgTimerC;
	SRTreeC.SelectLeaderTimer   -> SelectLeaderTimerC;     //<-------------------------------
	SRTreeC.SendDataTimer       -> SendDataTimerC;         //<-------------------------------
	SRTreeC.ForwardDataTimer    -> ForwardDataTimerC;      //<-------------------------------
	SRTreeC.SendDataRandomTimer -> SendDataRandomTimerC;   //<-------------------------------
	SRTreeC.LeaderResponceTimer -> LeaderResponceTimerC;   //<-------------------------------




	
	SRTreeC.RoutingPacket->RoutingSenderC.Packet;
	SRTreeC.RoutingAMPacket->RoutingSenderC.AMPacket;
	SRTreeC.RoutingAMSend->RoutingSenderC.AMSend;
	SRTreeC.RoutingReceive->RoutingReceiverC.Receive;

	SRTreeC.IAMLEADERPacket   -> IAMLEADERSenderC.Packet;     //<-------------------------------
	SRTreeC.IAMLEADERAMPacket -> IAMLEADERSenderC.AMPacket;   //<-------------------------------
	SRTreeC.IAMLEADERAMSend   -> IAMLEADERSenderC.AMSend;     //<-------------------------------
	SRTreeC.IAMLEADERReceive  -> IAMLEADERReceiverC.Receive;  //<-------------------------------

	SRTreeC.DATAPacket   -> DATASenderC.Packet;     //<-------------------------------
	SRTreeC.DATAAMPacket -> DATASenderC.AMPacket;   //<-------------------------------
	SRTreeC.DATAAMSend   -> DATASenderC.AMSend;     //<-------------------------------
	SRTreeC.DATAReceive  -> DATAReceiverC.Receive;  //<-------------------------------
	
	SRTreeC.NotifyPacket->NotifySenderC.Packet;
	SRTreeC.NotifyAMPacket->NotifySenderC.AMPacket;
	SRTreeC.NotifyAMSend->NotifySenderC.AMSend;
	SRTreeC.NotifyReceive->NotifyReceiverC.Receive;
	

	SRTreeC.RoutingSendQueue->RoutingSendQueueC;
	SRTreeC.RoutingReceiveQueue->RoutingReceiveQueueC;
	SRTreeC.NotifySendQueue->NotifySendQueueC;
	SRTreeC.NotifyReceiveQueue->NotifyReceiveQueueC;

	SRTreeC.IAMLEADERSendQueue    -> IAMLEADERSendQueueC;      //<-------------------------------
	SRTreeC.IAMLEADERReceiveQueue -> IAMLEADERReceiveQueueC;   //<-------------------------------

	SRTreeC.DATASendQueue    -> DATASendQueueC;      //<-------------------------------
	SRTreeC.DATAReceiveQueue -> DATAReceiveQueueC;   //<-------------------------------
	
	
}
