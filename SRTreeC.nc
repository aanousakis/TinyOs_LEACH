#include "SimpleRoutingTree.h"

module SRTreeC
{
	uses interface Boot;
	uses interface SplitControl as RadioControl;


	uses interface AMSend as RoutingAMSend;
	uses interface AMPacket as RoutingAMPacket;
	uses interface Packet as RoutingPacket;

	uses interface AMSend   as IAMLEADERAMSend;     //<----------------------
	uses interface AMPacket as IAMLEADERAMPacket;   //<----------------------
	uses interface Packet   as IAMLEADERPacket;     //<----------------------
	
	uses interface AMSend   as DATAAMSend;     //<----------------------
	uses interface AMPacket as DATAAMPacket;   //<----------------------
	uses interface Packet   as DATAPacket;     //<----------------------
	

	uses interface AMSend as NotifyAMSend;
	uses interface AMPacket as NotifyAMPacket;
	uses interface Packet as NotifyPacket;
	
	uses interface Timer<TMilli> as RoutingMsgTimer;
	uses interface Timer<TMilli> as SelectLeaderTimer;     //<----------------------
	uses interface Timer<TMilli> as SendDataTimer;         //<----------------------
	uses interface Timer<TMilli> as ForwardDataTimer;      //<----------------------
	uses interface Timer<TMilli> as SendDataRandomTimer;   //<----------------------
	uses interface Timer<TMilli> as LeaderResponceTimer;   //<----------------------


	uses interface Receive as RoutingReceive;
	uses interface Receive as NotifyReceive;
	uses interface Receive as IAMLEADERReceive;       //<----------------------
	uses interface Receive as DATAReceive;            //<----------------------


	uses interface PacketQueue as RoutingSendQueue;
	uses interface PacketQueue as RoutingReceiveQueue;
	
	uses interface PacketQueue as NotifySendQueue;
	uses interface PacketQueue as NotifyReceiveQueue;

	uses interface PacketQueue as IAMLEADERSendQueue;      //<----------------------
	uses interface PacketQueue as IAMLEADERReceiveQueue;   //<----------------------

	uses interface PacketQueue as DATASendQueue;      //<----------------------
	uses interface PacketQueue as DATAReceiveQueue;   //<----------------------
}
implementation
{
	message_t radioRoutingSendPkt;
	message_t radioNotifySendPkt;
	message_t radioIAMLEADERSendPkt;         //<------------------------------
	message_t radioDATASendPkt;              //<------------------------------
	
	
	bool RoutingSendBusy=FALSE;
	bool NotifySendBusy=FALSE;
	bool IAMLEADERSendBusy=FALSE;     //<------------------------------
	bool DATASendBusy=FALSE;          //<------------------------------

	
	bool lostRoutingSendTask=FALSE;
	bool lostNotifySendTask=FALSE;
	bool lostRoutingRecTask=FALSE;
	bool lostNotifyRecTask=FALSE;
	
	uint8_t curdepth;
	uint16_t parentID;
	uint8_t leaderID;


	uint8_t guros_cnt;
	uint8_t epoxi_cnt;

	uint8_t  data_cnt;
	uint16_t sumx;
	uint16_t sum2x;
	uint8_t  data_cnt_final;
	uint16_t sumx_final;
	uint16_t sum2x_final;

	uint8_t LEADERS[MAXLEADER];
	uint8_t leaders_cnt;

	uint8_t Pa=1;
	uint8_t Pb=4; 
	float P = 0.25;

	bool wasLeader=FALSE;

	task void sendRoutingTask();
	task void sendNotifyTask();
	task void receiveRoutingTask();
	task void receiveNotifyTask();
	task void sendIAMLEADERTask();
	task void receiveIAMLEADERTask();
	task void sendDATATask();
	task void receiveDATATask();

	void setLostRoutingSendTask(bool state)
	{
		atomic{
			lostRoutingSendTask=state;
		}
		if(state==TRUE)
		{
			//call Leds.led2On();
		}
		else 
		{
			//call Leds.led2Off();
		}
	}
	
	void setLostNotifySendTask(bool state)
	{
		atomic{
		lostNotifySendTask=state;
		}
		
		if(state==TRUE)
		{
			//call Leds.led2On();
		}
		else 
		{
			//call Leds.led2Off();
		}
	}
	
	void setLostNotifyRecTask(bool state)
	{
		atomic{
		lostNotifyRecTask=state;
		}
	}
	
	void setLostRoutingRecTask(bool state)
	{
		atomic{
		lostRoutingRecTask=state;
		}
	}
	void setRoutingSendBusy(bool state)
	{
		atomic{
		RoutingSendBusy=state;
		}
		if(state==TRUE)
		{
			// call Leds.led0On();
			// call Led0Timer.startOneShot(TIMER_LEDS_MILLI);
		}
		else 
		{
			//call Leds.led0Off();
		}
	}

	void setIAMLEADERSendBusy(bool state)
	{
		atomic{
		IAMLEADERSendBusy=state;
		}
		if(state==TRUE)
		{
			// call Leds.led0On();
			// call Led0Timer.startOneShot(TIMER_LEDS_MILLI);
		}
		else 
		{
			//call Leds.led0Off();
		}
	}

	void setDATASendBusy(bool state)
	{
		atomic{
		IAMLEADERSendBusy=state;
		}
		if(state==TRUE)
		{
			// call Leds.led0On();
			// call Led0Timer.startOneShot(TIMER_LEDS_MILLI);
		}
		else 
		{
			//call Leds.led0Off();
		}
	}
	
	void setNotifySendBusy(bool state)
	{
		atomic{
		NotifySendBusy=state;
		}
		dbg("Q","NotifySendBusy = %s\n", (state == TRUE)?"TRUE":"FALSE");
		
		if(state==TRUE)
		{
			// call Leds.led1On();
			// call Led1Timer.startOneShot(TIMER_LEDS_MILLI);
		}
		else 
		{
			//call Leds.led1Off();
		}
	}

	//enarxi tou kombou kai arxikopoiisi
	event void Boot.booted()
	{
		/////// arxikopoiisi radio kai serial
		call RadioControl.start();
		
		setRoutingSendBusy(FALSE);
		setNotifySendBusy(FALSE);
		
		guros_cnt =  0;
		epoxi_cnt =  0;
		leaderID  = -1;
	    data_cnt  =  0;
		sumx      =  0;
		sum2x     =  0;
		data_cnt_final  =  0;
		sumx_final      =  0;
		sum2x_final     =  0;

		memset(LEADERS, -1, MAXLEADER);
		leaders_cnt = 0;

		
		if(TOS_NODE_ID==0)
		{

			curdepth=0;
			parentID=0;
			dbg("Boot", "curdepth = %d  ,  parentID= %d \n", curdepth , parentID);

		}
		else
		{
			curdepth=-1;
			parentID=-1;
			dbg("Boot", "curdepth = %d  ,  parentID= %d \n", curdepth , parentID);
		}
	}
	
	event void RadioControl.startDone(error_t err)
	{
		if (err == SUCCESS)
		{
			dbg("Radio" , "Radio initialized successfully!!!\n");
			
			if (TOS_NODE_ID==0)
			{
				call RoutingMsgTimer.startOneShot(BOOT_DELAY);
			}
		}
		else
		{
			dbg("Radio" , "Radio initialization failed! Retrying...\n");
			call RadioControl.start();
		}
	}
	
	event void RadioControl.stopDone(error_t err)
	{ 
		dbg("Radio", "Radio stopped!\n");

	}
	
	//o RoutingMsgTimer tha xtupisei mono gia ti riza
	//otan xtupisei ftiaxnei to proto RoutingMsg kai to kanei broadcast
	event void RoutingMsgTimer.fired()
	{
		message_t tmp;
		error_t enqueueDone;
		
		RoutingMsg* mrpkt;
		
		if (TOS_NODE_ID==0)
		{
			dbg("SRTreeC", "####################################################\n");
			dbg("SRTreeC", "#######   Tree Initialization Start   ##############\n");
			dbg("SRTreeC", "####################################################\n");	
		}
		
		if(call RoutingSendQueue.full())
		{
			return;
		}
		
		
		mrpkt = (RoutingMsg*) (call RoutingPacket.getPayload(&tmp, sizeof(RoutingMsg)));
		if(mrpkt==NULL)
		{
			dbg("SRTreeC","RoutingMsgTimer.fired(): No valid payload... \n");
			return;
		}
		atomic{
		mrpkt->senderID=TOS_NODE_ID;
		mrpkt->depth = curdepth;
		}
		dbg("SRTreeC" , "In RoutingMsgTimer.fired()    : Sending RoutingMsg\n");
		
		call RoutingAMPacket.setDestination(&tmp, AM_BROADCAST_ADDR);
		call RoutingPacket.setPayloadLength(&tmp, sizeof(RoutingMsg));
		
		enqueueDone=call RoutingSendQueue.enqueue(tmp);
		
		if( enqueueDone==SUCCESS)
		{
			if (call RoutingSendQueue.size()==1)
			{
				dbg("Q" , "In RoutingMsgTimer.fired() : sendRoutingTask posted!!\n");
				post sendRoutingTask();
			}
			
			dbg("Q" , "In RoutingMsgTimer.fired() : RoutingMsg enqueued successfully in SendingQueue!!!\n");
		}
		else
		{
			dbg("SRTreeC","RoutingMsg failed to be enqueued in SendingQueue!!!");
		}		
	}
	
	event void RoutingAMSend.sendDone(message_t * msg , error_t err)
	{
		dbg("Q", "A Routing package sent... %s \n",(err==SUCCESS)?"True":"False");
		
		dbg("Q" , "Package sent %s \n", (err==SUCCESS)?"True":"False");
		setRoutingSendBusy(FALSE);
		
		if(!(call RoutingSendQueue.empty()))
		{
			post sendRoutingTask();
		}
	}
	
	event void NotifyAMSend.sendDone(message_t *msg , error_t err)
	{
		dbg("Q", "A Notify package sent... %s \n",(err==SUCCESS)?"True":"False");
		
	
		dbg("Q" , "Package sent %s \n", (err==SUCCESS)?"True":"False");

		setNotifySendBusy(FALSE);
		
		if(!(call NotifySendQueue.empty()))
		{
			post sendNotifyTask();
		}
	}

	
	event message_t* NotifyReceive.receive( message_t* msg , void* payload , uint8_t len)
	{
		error_t enqueueDone;
		message_t tmp;
		uint16_t msource;
		
		msource = call NotifyAMPacket.source(msg);
		
		dbg("SRTreeC", "In NotifyReceive.receive()     : Received NotifyParentMsg from %u  forwarded from %u \n",((NotifyParentMsg*) payload)->senderID, msource);

		atomic{
		memcpy(&tmp,msg,sizeof(message_t));
		//tmp=*(message_t*)msg;
		}
		enqueueDone=call NotifyReceiveQueue.enqueue(tmp);
		
		if( enqueueDone== SUCCESS)
		{
			post receiveNotifyTask();
		}
		else
		{
			dbg("SRTreeC","NotifyMsg enqueue failed!!! \n");		
		}
		return msg;
	}

	event message_t* RoutingReceive.receive( message_t * msg , void * payload, uint8_t len)
	{
		error_t enqueueDone;
		message_t tmp;
		uint16_t msource;
		
		msource =call RoutingAMPacket.source(msg);
			
		dbg("SRTreeC", "In RoutingReceive.receive()   : Received RoutingMsg from %u\n", msource);
		
		atomic{
		memcpy(&tmp,msg,sizeof(message_t));
		//tmp=*(message_t*)msg;
		}
		enqueueDone=call RoutingReceiveQueue.enqueue(tmp);
		if(enqueueDone == SUCCESS)
		{
			post receiveRoutingTask();
		}
		else
		{
			dbg("SRTreeC","RoutingMsg enqueue failed!!! \n");		
		}
		
		return msg;
	}
	
	
	////////////// Tasks implementations //////////////////////////////
	
	//to task ekteleitai kathe fora pou stelenetai ena RoutingMsg
	//episis arxizoume tous metrites gia tis treis faseis tou LEACH
	task void sendRoutingTask()
	{
		//uint8_t skip;
		uint8_t mlen;
		uint16_t mdest;
		error_t sendDone;
		//message_t radioRoutingSendPkt;                                                
		
		if (call RoutingSendQueue.empty())
		{
			dbg("SRTreeC","sendRoutingTask(): Q is empty!\n");
			return;
		}
		
		
		if(RoutingSendBusy)
		{
			dbg("SRTreeC","sendRoutingTask(): RoutingSendBusy= TRUE!!!\n");
			setLostRoutingSendTask(TRUE);
			return;   
		}
		
		radioRoutingSendPkt = call RoutingSendQueue.dequeue();
		
		
		mlen= call RoutingPacket.payloadLength(&radioRoutingSendPkt);
		mdest=call RoutingAMPacket.destination(&radioRoutingSendPkt);
		if(mlen!=sizeof(RoutingMsg))
		{
			dbg("SRTreeC","\t\tsendRoutingTask(): Unknown message!!!\n");
			return;
		}
		sendDone=call RoutingAMSend.send(mdest,&radioRoutingSendPkt,mlen);

		//xekiname tous metrites
		call SelectLeaderTimer.startPeriodicAt(TREE_INIT, EPOXI);
		call SendDataTimer.startPeriodicAt(TREE_INIT + SelectLeader, EPOXI);
		call ForwardDataTimer.startPeriodicAt(TREE_INIT + SelectLeader + SendData, EPOXI);
		
		if ( sendDone== SUCCESS)
		{
			dbg("SRTreeC","In sendRoutingTask()          : Node %d sent a RoutingMsg\n", TOS_NODE_ID);
			setRoutingSendBusy(TRUE);
		}
		else
		{
			dbg("SRTreeC","send failed!!!\n");
			//setRoutingSendBusy(FALSE);
		}
	}
	/**
	 * dequeues a message and sends it
	 */
	task void sendNotifyTask()
	{
		uint8_t mlen;//, skip;
		error_t sendDone;
		uint16_t mdest;
		NotifyParentMsg* mpayload;
		
		//message_t radioNotifySendPkt;
		
		if (call NotifySendQueue.empty())
		{
			dbg("SRTreeC","sendNotifyTask(): Q is empty!\n");
			return;
		}
		
		if(NotifySendBusy==TRUE)
		{
			dbg("SRTreeC","sendNotifyTask(): NotifySendBusy= TRUE!!!\n");
			setLostNotifySendTask(TRUE);
			return;
		}
		
		radioNotifySendPkt = call NotifySendQueue.dequeue();
		
		mlen=call NotifyPacket.payloadLength(&radioNotifySendPkt);
		
		mpayload= call NotifyPacket.getPayload(&radioNotifySendPkt,mlen);
		
		if(mlen!= sizeof(NotifyParentMsg))
		{
			dbg("SRTreeC", "\t\t sendNotifyTask(): Unknown message!!\n");
			return;
		}
		

		mdest= call NotifyAMPacket.destination(&radioNotifySendPkt);
		sendDone=call NotifyAMSend.send(mdest,&radioNotifySendPkt, mlen);
		
		if ( sendDone== SUCCESS)
		{
			dbg("Q","sendNotifyTask(): Send returned success!!!\n");
			setNotifySendBusy(TRUE);
		}
		else
		{
			dbg("SRTreeC","send failed!!!\n");
			//setNotifySendBusy(FALSE);
		}
	}
	////////////////////////////////////////////////////////////////////
	//*****************************************************************/
	///////////////////////////////////////////////////////////////////
	/**
	 * dequeues a message and processes it
	 */
	
	//an enas kombos den exei arxikopoiithei, epilegei os patera ton proto pou tha tou steilei RoutingMsg
	//kai kanei broadcast ena diko tou mhnyma 
	task void receiveRoutingTask()
	{
		message_t tmp;
		uint8_t len;
		message_t radioRoutingRecPkt;

		RoutingMsg* mrpkt;
		error_t enqueueDone;
		
		radioRoutingRecPkt= call RoutingReceiveQueue.dequeue();
		len= call RoutingPacket.payloadLength(&radioRoutingRecPkt);
				
		if(len == sizeof(RoutingMsg))
		{
			RoutingMsg * mpkt = (RoutingMsg*) (call RoutingPacket.getPayload(&radioRoutingRecPkt,len));
			

			dbg("SRTreeC" , "In receiveRoutingTask()       : Received RoutingMsg with senderID= %d , depth= %d \n", mpkt->senderID , mpkt->depth);

			if ( (parentID<0)||(parentID>=65535))//an den exei epilexei patera
			{
				// tote den exei akoma patera
				parentID= call RoutingAMPacket.source(&radioRoutingRecPkt);//mpkt->senderID;q
				curdepth= mpkt->depth + 1;

				dbg("SRTreeC","In receiveRoutingTask()       : Node %d selected Node %d as his father\n", TOS_NODE_ID, parentID);

				//dhmiourgia RoutingMSg 
				mrpkt = (RoutingMsg*) (call RoutingPacket.getPayload(&tmp, sizeof(RoutingMsg)));
				if(mrpkt==NULL)
				{
					dbg("SRTreeC","receiveRoutingTask() : No valid payload... \n");
					return;
				}

				atomic
				{
					mrpkt->senderID=TOS_NODE_ID;
					mrpkt->depth = curdepth;
				}

				call RoutingAMPacket.setDestination(&tmp, AM_BROADCAST_ADDR);
				call RoutingPacket.setPayloadLength(&tmp, sizeof(RoutingMsg));

				enqueueDone=call RoutingSendQueue.enqueue(tmp);

				if( enqueueDone==SUCCESS)
				{
					if (call RoutingSendQueue.size()==1)
					{
						post sendRoutingTask();
						dbg("Q","receiveRoutingTask() : Message enqueued & task posted\n");
					}
				}
				else
				{
					dbg("Q","receiveRoutingTask() : RoutingMsg failed to be enqueued in SendingQueue!!!");
				}

			}
			dbg("SRTreeC","In receiveRoutingTask()       : Node %d has Node %d as his father\n", TOS_NODE_ID, parentID);
		}
		else
		{
			dbg("SRTreeC","receiveRoutingTask():Empty message!!! \n");

			setLostRoutingRecTask(TRUE);
			return;
		}
		
	}


////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////	
	
	//i riza sunathroizei ta dedomena apo olous tous cluster kai upologizei to AVG kai VARIANCE gia olo to dentro
	//oloi oi alloi komboi proothoun to NotifyParentMsg ston patera tous mexri na ftasei sti riza
	task void receiveNotifyTask()
	{
		message_t tmp;
		uint8_t len;
		message_t radioNotifyRecPkt;
		

		radioNotifyRecPkt= call NotifyReceiveQueue.dequeue();
		
		len= call NotifyPacket.payloadLength(&radioNotifyRecPkt);

		if(len == sizeof(NotifyParentMsg))
		{
			// an to parentID== TOS_NODE_ID tote
			// tha proothei to minima pros tin riza xoris broadcast
			// kai tha ananeonei ton tyxon pinaka paidion..
			// allios tha diagrafei to paidi apo ton pinaka paidion
			
			NotifyParentMsg* mr = (NotifyParentMsg*) (call NotifyPacket.getPayload(&radioNotifyRecPkt,len));
			

			if ( TOS_NODE_ID==0)
			{
					dbg("SRTreeC" , "In receiveNotifyTask()         : Root received NotifyParentMsg from Cluster leader %d\n", mr->senderID);

					sumx_final     = sumx_final     + mr->sumx;
					sum2x_final    = sum2x_final    + mr->sum2x;
					data_cnt_final = data_cnt_final + mr->cnt;
			}
			else
			{
				NotifyParentMsg* m;
				memcpy(&tmp,&radioNotifyRecPkt,sizeof(message_t));

				dbg("SRTreeC" , "In receiveNotifyTask()         : NotifyParentMsg received from Cluster leader %d\n", mr->senderID);
				
				m = (NotifyParentMsg *) (call NotifyPacket.getPayload(&tmp, sizeof(NotifyParentMsg)));
				
				
				dbg("SRTreeC" , "In receiveNotifyTask()         : Forwarding NotifyParentMsg from senderID= %d  to parentID=%d \n" , m->senderID, parentID);

				call NotifyAMPacket.setDestination(&tmp, parentID);
				call NotifyPacket.setPayloadLength(&tmp,sizeof(NotifyParentMsg));
				
				if (call NotifySendQueue.enqueue(tmp)==SUCCESS)
				{
					dbg("Q", "receiveNotifyTask(): NotifyParentMsg enqueued in SendingQueue successfully!!!\n");
					if (call NotifySendQueue.size() == 1)
					{
						post sendNotifyTask();
					}
				}				
			}		
		}
		else
		{
			dbg("SRTreeC","receiveNotifyTask():Empty message!!! \n");
			setLostNotifyRecTask(TRUE);
			return;
		}
		
	}
	//upologizoume to Tn gia na doume an tha ginei leader
	//an ginei leader to diafimizei me ena broadcast
	event void SelectLeaderTimer.fired()
	{

		uint8_t Tn;
		uint8_t random1;

		IAMLEADERMsg* mrpkt;
		error_t enqueueDone;
		message_t tmp;

		if (epoxi_cnt != 5)//se kathe nea epoxi enos gurou
		{
			epoxi_cnt = epoxi_cnt +1;
		}
		else
		{// se kathe guro
			epoxi_cnt = 1;
			guros_cnt = guros_cnt + 1;
			leaderID=-1;

					//pote tha stamatisei to programma
					if (guros_cnt == GUROI_EPAN)
					{
						dbg("SRTreeC","SelectLeaderTimer.fired() :Stoping Timers\n"); 
						call SelectLeaderTimer.stop();
						call SendDataTimer.stop();
						call ForwardDataTimer.stop();
						//dbg("SRTreeC","SelectLeaderTimer.fired() : Timer fired Guros = %d, Epoxi = %d\n", guros_cnt, epoxi_cnt);
						return;
					}
		}

		dbg("SRTreeC","In SelectLeaderTimer.fired()  : Guros = %d, Epoxi = %d, wasLeader %d\n", guros_cnt, epoxi_cnt, wasLeader==TRUE);

		if (epoxi_cnt == 1)//epilegoume leader se kathe guro, dhladh opote i epoxi einai 1
		{
			dbg("SRTreeC","In SelectLeaderTimer.fired()  : Cluster leader selection\n"); 

			//otan perasoun 4 guroi, oloi oi komboi einai upopsifioi leaders
			if ( ((guros_cnt % 4) == 0 ) && (guros_cnt != 0) )
			{
				wasLeader = FALSE;
				dbg("SRTreeC","SelectLeaderTimer.fired() : Timer fired Guros = %d, Epoxi = %d, wasLeader %d\n", guros_cnt, epoxi_cnt, wasLeader==TRUE);
			}	


			if( (! wasLeader) || (TOS_NODE_ID == 0) )
			{
				//upologsizoume to tn kai blepoume an tha ginei cluster leader
				Tn = 100 * P/(1-P*(guros_cnt % (Pb)));

				srand (time(NULL) + TOS_NODE_ID);
				random1 = rand() % 100;

				//dbg("SRTreeC","SelectLeaderTimer.fired() :random = %d , Tn = %d\n", random1,  Tn);
				
				//otan o kombos ginei cluster leader, i riza einai panta leader
				if( (random1 <= Tn) || (TOS_NODE_ID == 0) )
				{
					dbg("SRTreeC","In SelectLeaderTimer.fired()  : Node %d is cluster leader\n", TOS_NODE_ID);
					wasLeader = TRUE;

					leaderID = TOS_NODE_ID;

				//kanei broadcast sto cluster tou

					dbg("SRTreeC","In SelectLeaderTimer.fired()  : Node %d Broadcasts IAMLEADERMsg\n");

					//dhmiourgia IAMLEADERMSg 
					mrpkt = (IAMLEADERMsg*) (call IAMLEADERPacket.getPayload(&tmp, sizeof(IAMLEADERMsg)));
					if(mrpkt==NULL)
					{
						dbg("SRTreeC","SelectLeaderTimer.fired() : No valid payload... \n");
						return;
					}

					atomic
					{
						//mrpkt->senderID=TOS_NODE_ID;
					}

					call IAMLEADERAMPacket.setDestination(&tmp, AM_BROADCAST_ADDR);
					call IAMLEADERPacket.setPayloadLength(&tmp, sizeof(IAMLEADERMsg));

					enqueueDone=call IAMLEADERSendQueue.enqueue(tmp);

					if( enqueueDone==SUCCESS)
					{
						if (call IAMLEADERSendQueue.size()==1)
						{
							post sendIAMLEADERTask();
							dbg("Q","SelectLeaderTimer.fired() : Message enqueued & task posted\n");
						}
					}
					else
					{
						dbg("SRTreeC","SelectLeaderTimer.fired() : IAMLEADERMsg failed to be enqueued in SendingQueue!!!");
					}
				}
				else
				{
					dbg("SRTreeC","In SelectLeaderTimer.fired()  : Node %d is not a cluster leader\n", TOS_NODE_ID);
				}
			}
		}
	}
	//arxika epilegoume enan apo tous upopsifious leaders h o idios kombos ginetai leader an den uparxoun upopsifioi
	//an o kombos einai leader pairnei mia metrisi kai tin sunathroizei
	//telos stelnoume tin metrisi ston leader, xrhsimopoioume tin rand gia na min steiloun oloi oi komboi tou cluster tautoxronna
	event void SendDataTimer.fired()
	{
		nx_uint32_t random;
		data_cnt = 0;
		sumx     = 0;
		sum2x    = 0;


		dbg("SRTreeC","In SendDataTimer.fired()      : Node %d selected node %d as leader.\n", TOS_NODE_ID, leaderID);

		// an kapios kombos den brisketai se kapoio cluster, epilegei ton eauto tou os cluster leader
		if ( (leaderID<0)||(leaderID>=255))
		{
			if (leaders_cnt > 0)//an uparxoun poloi upopsifioi leader epilegoume enan tuxaia
			{
				srand (time(NULL) + TOS_NODE_ID);
				leaderID = LEADERS[rand()%leaders_cnt];
				memset(LEADERS, -1, MAXLEADER);
				leaders_cnt = 0;
				dbg("SRTreeC","In SendDataTimer.fired()      : Node %d selected node %d as his leader\n", TOS_NODE_ID, leaderID);
			}
			else
			{
				leaderID = TOS_NODE_ID;
				dbg("SRTreeC","In SendDataTimer.fired()      : Node %d no more alone, He is leader!!!\n", TOS_NODE_ID);
			}
		}
		
		if (leaderID == TOS_NODE_ID)  //otan kapios kombos einai leader, den stelnei tin metrisi me mhnuma ston eauto tou, tin pairnei kateutheian
		{
			//metrisi
			random = TOS_NODE_ID+ rand() % 21;

			dbg("SRTreeC","In SendDataTimer.fired()      : Node %d metrisi = %d\n", TOS_NODE_ID, random);

			data_cnt = data_cnt + 1;                                    //<--------------------------------
	        sumx     = sumx + random;
		    sum2x    = sum2x + (random)*(random);
		}
		else //an enas kombos den einai o idios leader, tote steile tin metrisi ston leader
		{
			srand (time(NULL) + TOS_NODE_ID);
			call SendDataRandomTimer.startOneShot(rand()%SendData);
		}
	}
	//to teleutaio stadio tou Leach, xrisimopoioume ton metriti LeaderResponceTimer oste oi komboi tou epipedou N na ksipnisoun prin tous kombous tou epipedou N+1, epishs 
	//xrhsimopoioume tin rand gia na min ksipnisoun oi komboi tou idioy epipedoy tin idia stigmi
	event void ForwardDataTimer.fired()
	{

		if (leaderID == TOS_NODE_ID)
		{
			dbg("SRTreeC", "Cluster leader :%d Apotelesmata gia  GURO = %d  kai tin  EPOXI = %d\n", TOS_NODE_ID, guros_cnt, epoxi_cnt);
			dbg("SRTreeC", "In task ForwardDataTimer.fired() : : sum(x) = %d, sum(X^2) = %d, count = %d\n", sumx, sum2x, data_cnt );
			dbg("SRTreeC", "In task ForwardDataTimer.fired() : : AVG = %f, Variance =%f\n", (float) sumx/data_cnt, (float) sum2x/data_cnt - pow(  (float) sumx/data_cnt, 2));
		
			//dbg("SRTreeC", "ForwardData = %d, curdepth = %d, t = %d\n", ForwardData, curdepth, (ForwardData - (  transmissionWindow *  (curdepth +1) ) + rand()%transmissionWindow) );
			call LeaderResponceTimer.startOneShot(  (ForwardData - (  transmissionWindow *  (curdepth +1) ) + rand()%transmissionWindow) );
		}

		if (TOS_NODE_ID == 0 )
		{
			sumx_final     = sumx_final  + sumx;
			sum2x_final    = sum2x_final + sum2x;
			data_cnt_final = data_cnt_final + data_cnt;
		}

	}

	//otan xtupisei autos o timer, pairnoume mia metrisi kai ftiaxnoume ena DATAMSG
	event void SendDataRandomTimer.fired()
	{
		DATAMsg* mrpkt;
		error_t enqueueDone;
		message_t tmp;

		dbg("SRTreeC","In SendDataRandomTimer.fired(): Node %d send a DATAMsg to leader %d\n", TOS_NODE_ID, leaderID);

		//dhmiourgia DATAMSG
		mrpkt = (DATAMsg*) (call DATAPacket.getPayload(&tmp, sizeof(DATAMsg)));
		if(mrpkt==NULL)
		{
			dbg("SRTreeC","SendDataTimer.fired() : No valid payload... \n");
			return;
		}
		//metrisi
		atomic
		{
			mrpkt->data=TOS_NODE_ID+ rand() % 21;;   //<---------------------
			dbg("SRTreeC","In SendDataRandomTimer.fired()      : Node %d metrisi = %d\n", TOS_NODE_ID, mrpkt->data);
		}

		call DATAAMPacket.setDestination(&tmp, leaderID);
		call DATAPacket.setPayloadLength(&tmp, sizeof(DATAMsg));

		enqueueDone=call DATASendQueue.enqueue(tmp);

		if( enqueueDone==SUCCESS)
		{
			if (call DATASendQueue.size()==1)
			{
				post sendDATATask();
				dbg("Q","SendDataTimer.fired() : DATAmsg enqueued & sendDATATask posted\n");
			}
		}
		else
		{
			dbg("SRTreeC","SendDataTimer.fired() : DATAMsg failed to be enqueued in SendingQueue!!!");
		}
	}

	//prota tha ksipisoun ta fila
	//afou exoume sunathroisei tis metriseis, o leader dhmiourgei ena NotifyParentMsg kai to stelnei sti riza meso tou dentrou
	event void LeaderResponceTimer.fired()
	{
		NotifyParentMsg* m;
		message_t tmp;

		dbg("SRTreeC","In LeaderResponceTimer.fired() : Node %d sending NotifyParentMsg to parent node : %d\n", TOS_NODE_ID, parentID);

		if (TOS_NODE_ID == 0 )// otan xipsnisei i riza upologizei to teliko apotelesma kai to emfanizei
		{
			dbg("SRTreeC", "############################################################################ \n");
			dbg("SRTreeC", "#           Teliko apotelesma gia to GURO = %d  kai tin  EPOXI = %d        #\n", guros_cnt, epoxi_cnt);
			dbg("SRTreeC", "############################################################################ \n");
			dbg("SRTreeC", "In LeaderResponceTimer.fired() : sum(x) = %d, sum(X^2) = %d, count = %d\n", sumx_final, sum2x_final, data_cnt_final );
			dbg("SRTreeC", "In LeaderResponceTimer.fired() : AVG = %f, Variance =%f\n", (float) sumx_final/data_cnt_final, (float) sum2x_final/data_cnt_final - pow(  (float) sumx_final/data_cnt_final, 2));
			dbg("SRTreeC", "############################################################################ \n");


			sumx_final     = 0;
			sum2x_final    = 0;
			data_cnt_final = 0;

		}
		else
		{
		//dhmiourgia mhnymatos notifyparent 
			//kataskeuazoume to minima me tis metriseis pou tha steiloume ston patera
			m = (NotifyParentMsg *) (call NotifyPacket.getPayload(&tmp, sizeof(NotifyParentMsg)));

			atomic
			{
				m->senderID  = TOS_NODE_ID;
				m->depth     = curdepth;
				m->parentID  = parentID;
				m->sumx      = sumx;
		 		m->sum2x     = sum2x;
		 		m->cnt       = data_cnt;
			}

			call NotifyAMPacket.setDestination(&tmp, parentID);
			call NotifyPacket.setPayloadLength(&tmp,sizeof(NotifyParentMsg));

			if (call NotifySendQueue.enqueue(tmp)==SUCCESS)
			{
				dbg("Q", "LeaderResponceTimer.fired() : NotifyParentMsg enqueued in SendingQueue successfully!!!\n");
				if (call NotifySendQueue.size() == 1)
				{
					post sendNotifyTask();
					dbg("Q", "LeaderResponceTimer.fired() : sendNotifyTask() task posted)\n");
				}
			}
		}
	}

	//lipsi mhnymatos IAMLEADERMsg
	event message_t* IAMLEADERReceive.receive( message_t * msg , void * payload, uint8_t len)
	{
		error_t enqueueDone;
		message_t tmp;
		uint16_t msource;
		
		msource =call IAMLEADERAMPacket.source(msg);
		
		dbg("SRTreeC", "In IAMLEADERReceive.receive() : Received IAMLEADERMsg from node  %u\n",msource);
		//dbg("SRTreeC", "Something received!!!\n");
		
		atomic{
		memcpy(&tmp,msg,sizeof(message_t));
		//tmp=*(message_t*)msg;
		}
		enqueueDone=call IAMLEADERReceiveQueue.enqueue(tmp);
		if(enqueueDone == SUCCESS)
		{

			post receiveIAMLEADERTask();
		}
		else
		{
			dbg("SRTreeC","IAMLEADERMsg enqueue failed!!! \n");		
		}
		return msg;
	}

	event void IAMLEADERAMSend.sendDone(message_t * msg , error_t err)
	{
		dbg("Q", "A IAMLEADER package sent... %s \n",(err==SUCCESS)?"True":"False");
		
		dbg("Q" , "Package sent %s \n", (err==SUCCESS)?"True":"False");
		setIAMLEADERSendBusy(FALSE);
		
		if(!(call IAMLEADERSendQueue.empty()))
		{
			post sendIAMLEADERTask();
		}
	}

	//Oi upopsifioi cluster leaders mpainoun se ena pinaka oste argotera na epilegei enas apo autous
	task void receiveIAMLEADERTask()
	{
		uint8_t len;
		message_t radioIAMLEADERRecPkt;
		uint16_t msource;

		radioIAMLEADERRecPkt= call IAMLEADERReceiveQueue.dequeue();
		len= call IAMLEADERPacket.payloadLength(&radioIAMLEADERRecPkt);

		msource = call IAMLEADERAMPacket.source(&radioIAMLEADERRecPkt);

		if(len == sizeof(IAMLEADERMsg))
		{
			// if ( (leaderID<0)||(leaderID>=255))
			// {
			// 	leaderID = msource;
			// 	dbg("SRTreeC" , "receiveIAMLEADERTask() : Node %d selects Node %d as Cluster leader\n", TOS_NODE_ID, leaderID);
			// }
			if (leaderID == TOS_NODE_ID)
			{
				dbg("SRTreeC" , "In receiveIAMLEADERTask()     : Node %d has leader\n", msource, leaders_cnt);
			}
			else
			{
				LEADERS[leaders_cnt] = msource;
				leaders_cnt = leaders_cnt + 1;

				dbg("SRTreeC" , "In receiveIAMLEADERTask()     : Node %d is one of the %d leader candidates\n", msource, leaders_cnt);
			}
		}
	}

	// apostoli IAMLEADERMsg mhnymatos
	task void sendIAMLEADERTask()
	{
		uint8_t mlen;
		uint16_t mdest;
		error_t sendDone;

		if (call IAMLEADERSendQueue.empty())
		{
			dbg("SRTreeC","sendIAMLEADERTask() : Q is empty!\n");
			return;
		}

		if(IAMLEADERSendBusy)
		{
			dbg("SRTreeC","sendIAMLEADERTask() : IAMLEADERSendBusy= TRUE!!!\n");
			return;
		}
		
		radioIAMLEADERSendPkt = call IAMLEADERSendQueue.dequeue();

		mlen= call IAMLEADERPacket.payloadLength(&radioIAMLEADERSendPkt);
		mdest=call IAMLEADERAMPacket.destination(&radioIAMLEADERSendPkt);
		
		if(mlen!=sizeof(IAMLEADERMsg))
		{
			dbg("SRTreeC","sendIAMLEADERTask(): Unknown message!!!\n");
			return;
		}
		sendDone=call IAMLEADERAMSend.send(mdest,&radioIAMLEADERSendPkt,mlen);

		if ( sendDone== SUCCESS)
		{
			dbg("Q","sendIAMLEADERTask() : Send returned success!!!\n");
			setRoutingSendBusy(TRUE);
		}
		else
		{
			dbg("SRTreeC","sendIAMLEADERTask()send failed!!!\n");
		}
	}

	//lipsi mhnymatos DATAMsg
	event message_t* DATAReceive.receive( message_t * msg , void * payload, uint8_t len)
	{
		error_t enqueueDone;
		message_t tmp;
		uint16_t msource;
	
		msource =call DATAAMPacket.source(msg);
		
		dbg("SRTreeC", "In DATAReceive.receive()      : Received DATAMsg from %u\n", msource);
		//dbg("SRTreeC", "Something received!!!\n");
		
		atomic{
		memcpy(&tmp,msg,sizeof(message_t));
		//tmp=*(message_t*)msg;
		}
		enqueueDone=call DATAReceiveQueue.enqueue(tmp);
		if(enqueueDone == SUCCESS)
		{

			post receiveDATATask();
		}
		else
		{
			dbg("SRTreeC","DATAMsg enqueue failed!!! \n");		
		}
		return msg;
	}

	event void DATAAMSend.sendDone(message_t * msg , error_t err)
	{
		dbg("Q", "A DATA package sent... %s \n",(err==SUCCESS)?"True":"False");
		
		dbg("Q" , "Package sent %s \n", (err==SUCCESS)?"True":"False");
		setDATASendBusy(FALSE);
		
		if(!(call DATASendQueue.empty()))
		{
			post sendDATATask();
		}
	}

	//apostoli mhnymatos DATAMsg
	task void sendDATATask()
	{
		uint8_t mlen;//, skip;
		error_t sendDone;
		uint16_t mdest;
		DATAMsg* mpayload;
		

		
		if (call DATASendQueue.empty())
		{
			dbg("SRTreeC","sendDATATask(): Q is empty!\n");
			return;
		}
		
		if(DATASendBusy==TRUE)
		{
			dbg("SRTreeC","sendDATATask(): DATASendBusy= TRUE!!!\n");
			return;
		}
		
		radioDATASendPkt = call DATASendQueue.dequeue();
		
		mlen=call DATAPacket.payloadLength(&radioDATASendPkt);
		
		mpayload= call DATAPacket.getPayload(&radioDATASendPkt,mlen);
		
		if(mlen!= sizeof(DATAMsg))
		{
			dbg("SRTreeC", "sendDATATask(): Unknown message!!\n");
			return;
		}
		
		mdest= call DATAAMPacket.destination(&radioDATASendPkt);
		sendDone=call DATAAMSend.send(mdest,&radioDATASendPkt, mlen);
		
		if ( sendDone== SUCCESS)
		{
			dbg("Q","sendDATATask(): Send returned success!!!\n");
			setDATASendBusy(TRUE);
		}
		else
		{
			dbg("SRTreeC","sendDATATask(): Send failed!!!\n");
			//setNotifySendBusy(FALSE);
		}
	}

	//sto task auto oi cluster leader sunathroizoun tis metriseis
	task void receiveDATATask()
	{
		uint8_t len;
		message_t radioDATARecPkt;
		uint16_t msource;
		
		radioDATARecPkt= call DATAReceiveQueue.dequeue();
		len= call DATAPacket.payloadLength(&radioDATARecPkt);

		if(len == sizeof(DATAMsg))
		{
			DATAMsg * mpkt = (DATAMsg*) (call DATAPacket.getPayload(&radioDATARecPkt,len));
			msource =call DATAAMPacket.source(&radioDATARecPkt);

			data_cnt = data_cnt + 1;
	        sumx     = sumx + mpkt->data;
		    sum2x    = sum2x + (mpkt->data)*(mpkt->data);

		    dbg("SRTreeC","In receiveDATATask()          : Cluster Leader %d received DATAMsg from node : %d, MSGcnt = %d\n", TOS_NODE_ID, msource, data_cnt);
		}
		

	}
}